const container = document.querySelector(".card-container");

const images = [
  { id: 1, image: "./fruits/bananas-icon.png" },
  { id: 2, image: "./fruits/blueberry-icon.png" },
  { id: 3, image: "./fruits/cherry-icon.png" },
  { id: 4, image: "./fruits/fruit-icon.png" },
  { id: 5, image: "./fruits/orange-icon.png" },
  { id: 6, image: "./fruits/pear-icon.png" },
  { id: 7, image: "./fruits/pumpkin-icon.png" },
  { id: 8, image: "./fruits/strawberry-fruit-icon.png" },
  { id: 1, image: "./fruits/bananas-icon.png" },
  { id: 2, image: "./fruits/blueberry-icon.png" },
  { id: 3, image: "./fruits/cherry-icon.png" },
  { id: 4, image: "./fruits/fruit-icon.png" },
  { id: 5, image: "./fruits/orange-icon.png" },
  { id: 6, image: "./fruits/pear-icon.png" },
  { id: 7, image: "./fruits/pumpkin-icon.png" },
  { id: 8, image: "./fruits/strawberry-fruit-icon.png" },
];

function shuffleArray(arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));

    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
}

shuffleArray(images);

images.forEach((item) => {
  const cardDiv = document.createElement("div");
  cardDiv.classList.add(`card`);
  cardDiv.id = item.id;

  const frontDiv = document.createElement("div");
  frontDiv.classList.add("front-face");
  frontDiv.id = item.id;

  const imageElement = document.createElement("img");
  imageElement.src = item.image;
  imageElement.classList.add("img");

  frontDiv.appendChild(imageElement);

  const backDiv = document.createElement("div");
  backDiv.classList.add("back-face");

  const blackImageElement = document.createElement("img");
  blackImageElement.src =
    "https://upload.wikimedia.org/wikipedia/commons/6/68/Solid_black.png";
  blackImageElement.classList.add("image");

  backDiv.appendChild(blackImageElement);

  cardDiv.appendChild(frontDiv);
  cardDiv.appendChild(backDiv);
  container.appendChild(cardDiv);
});

let timeContainer = document.querySelector(".time");
let moves = document.querySelector(".moves");
let sec = 0;
let min = 0;
let move = 0;

document.querySelector("#start").addEventListener("click", (event) => {
  // console.log(target, event);
  event.target.style.color = "grey";

  setInterval(() => {
    sec += 1;
    if (sec > 59) {
      sec = 0;
      min += 1;
      timeContainer.textContent = `time:${min} minutes ${sec} seconds`;
    } else {
      timeContainer.textContent = `time:${min} minutes ${sec} seconds`;
    }
  }, 1000);

  // const container = document.querySelectorAll(".card");
  // cards.forEach((card) => card.addEventListener("click", flipCard));

  container.addEventListener("click", (event) => {
    if (event.target.className === "image") {
      let card = event.target.parentNode.parentNode;
      let backFace = event.target.parentNode;
      flipCard(card, backFace);
    }
  });
});
// container.addEventListener("click", (event) => {
//   if(event.target.className === "image") {
//     let card = event.target.parentNode.parentNode;
//     let backFace = event.target.parentNode
//     flipCard(card, backFace);
//   }
// });

let clickedCard = [];
let matchedCard = [];

function flipCard(card, backSide) {
  if (clickedCard[0] === card) {
    card.classList.add("clicked");
    return;
  }

  move += 1;
  moves.textContent = `${move} moves`;

  card.classList.add("flip");

  backSide.style.display = "none";
  // console.log( target.parentNode);

  if (clickedCard.length === 0) {
    clickedCard.push(card);
    console.log(clickedCard);
  } else {
    clickedCard.push(card);
    console.log(clickedCard);

    if (clickedCard[0].id === clickedCard[1].id) {
      console.log(matchedCard);

      setTimeout(() => {
        clickedCard.forEach(function (card) {
          card.classList.add("matched");
          matchedCard.push(card);
          if (matchedCard.length === images.length) {
            console.log("yes");
            document.querySelector(".congrats").textContent =
              `Congratulations!!!, You cleared the game in ${min}minutes ${sec}secs time with only ${move} moves..`
              "Refresh the page to play again";
          }
          clickedCard = [];
        });
      }, 500);
    } else {
      clickedCard.forEach(function (card) {
        setTimeout(() => {
          card.classList.remove("flip");
          card.children[1].style.display = "contents";
        }, 1000);
        clickedCard = [];
      });

      // matchedCard.push(card);
    }
  }
}
